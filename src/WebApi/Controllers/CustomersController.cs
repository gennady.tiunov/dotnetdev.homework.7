using AutoMapper;
using Domain.BusinessLogic.Contracts;
using Microsoft.AspNetCore.Mvc;
using WebApi.Contracts.Entities;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerBusinessService _customerBusinessService;
        private readonly IMapper _mapper;

        public CustomersController(
            ICustomerBusinessService customerBusinessService,
            IMapper mapper) 
        {
            _customerBusinessService = customerBusinessService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomersAsync()
        {
            var entities = await _customerBusinessService.GetAllAsync();
            return Ok(_mapper.Map<IEnumerable<Customer>>(entities));
        }

        [HttpGet("{id:guid}")]   
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] Guid id)
        {
            var entity = await _customerBusinessService.GetByIdAsync(id);
            if (entity == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<Customer>(entity));
        }

        [HttpPost("")]   
        public async Task<ActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            var entity = await _customerBusinessService.GetByIdAsync(customer.Id);
            if (entity != null)
            {
                return Conflict();
            }

            entity = _mapper.Map<Domain.Entities.Customer>(customer);

            await _customerBusinessService.AddAsync(entity);

            var url = $"{ControllerContext.HttpContext.Request.Scheme}://{ControllerContext.HttpContext.Request.Host}/api/v1/Customers/{customer.Id}";

            return Created(url, customer.Id);
        }
    }
}