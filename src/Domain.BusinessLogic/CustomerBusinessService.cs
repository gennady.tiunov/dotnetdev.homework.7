﻿using AutoMapper;
using DataAccess.Contracts;
using Domain.BusinessLogic.Contracts;
using Domain.Entities;

namespace Domain.BusinessLogic
{
    public class CustomerBusinessService : ICustomerBusinessService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public CustomerBusinessService(
            ICustomerRepository customerRepository,
            IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            var dalEntitities = await _customerRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<Customer>>(dalEntitities);
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            var dalEntitity = await _customerRepository.GetByIdAsync(id);

            return _mapper.Map<Customer>(dalEntitity);
        }

        public async Task AddAsync(Customer entity)
        {
            var dalEntity = _mapper.Map<DataAccess.Entities.Customer>(entity);

            await _customerRepository.AddAsync(dalEntity);
        }

        public async Task UpdateAsync(Customer entity)
        {
            var dalEntity = _mapper.Map<DataAccess.Entities.Customer>(entity);

            await _customerRepository.UpdateAsync(dalEntity);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _customerRepository.DeleteAsync(id);
        }
    }
}
