﻿using AutoMapper;

namespace Domain.BusinessLogic.Mappings
{
    public class CustomerMappingProfile : Profile
    {
        public CustomerMappingProfile()
        {
            CreateMap<Entities.Customer, DataAccess.Entities.Customer>().ReverseMap();
        }
    }
}
