﻿using AutoMapper;

namespace WebApi
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection RegisterAutoMapping(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<Mappings.CustomerMappingProfile>();
                cfg.AddProfile<Domain.BusinessLogic.Mappings.CustomerMappingProfile>();
            });

            configuration.AssertConfigurationIsValid();

            return services.AddSingleton<IMapper>(new Mapper(configuration));
        }
    }
}