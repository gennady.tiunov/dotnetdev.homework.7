﻿namespace ConsoleApp
{
    public interface IRandomStringGenerator
    {
        string GenerateRandomString();
    }
}
