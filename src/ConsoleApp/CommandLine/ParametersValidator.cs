﻿using System;
using System.Linq;

namespace ConsoleApp.CommandLine
{
    public static class ParametersValidator
    {
        public static bool Validate(Parameters parameters, out string errorMessage)
        {
            const string message = "Validating entered parameters";
            Console.WriteLine(message);
            
            errorMessage = string.Empty;

            if (parameters.Commands.Count() != 1)
            {
                errorMessage = "Please, specify command correctly";
                return false;
            }

            if (parameters.Commands.Single() == Parameters.Command.GetCustomer &&
                parameters.CustomerId == Guid.Empty)
            {
                errorMessage = "Id is required to get customer";
                return false;
            }

            if (parameters.Commands.Single() == Parameters.Command.CreateCustomer &&
                parameters.CustomerId != Guid.Empty)
            {
                errorMessage = "Id must not be specified when creating customer";
                return false;
            }

            return true;
        }
    }
}
