﻿using CommandLine;
using ConsoleApp;
using ConsoleApp.CommandLine;
using ConsoleApp.Connectors;
using Infrastructure.EntityFramework;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Text.Json;
using WebApi.Contracts.Entities;

var hostBuilder = Host
    .CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.RegisterServices();
        services.AddHttpClient<ICustomerConnector, CustomerConnector>(client => client.BaseAddress = new Uri("http://localhost:5206"));
        services.AddSingleton<IRandomStringGenerator>(new RandomStringGenerator(15));
        services.AddSingleton<Program>();
    });

var host = hostBuilder.Build();
host.Services.GetRequiredService<Program>().Run(args);

public partial class Program
{
    private ICustomerConnector _customerConnector;
    private IRandomStringGenerator _randomStringGenerator;

    public Program(
        ICustomerConnector customerConnector,
        IRandomStringGenerator randomStringGenerator)
    {
        _customerConnector = customerConnector;
        _randomStringGenerator = randomStringGenerator;
    }

    public void Run(string[] args)
    {
        Parser.Default.ParseArguments<Parameters>(args)
            .WithParsed(parameters =>
            {
            if (!ParametersValidator.Validate(parameters, out var errorMessage))
            {
                Console.WriteLine(errorMessage);
                Environment.Exit(1);
            }

            switch (parameters.Commands.Single())
            {
                case Parameters.Command.GetCustomer:

                    Console.WriteLine($"Getting customer by Id = '{parameters.CustomerId}'");
                    var customer = _customerConnector.GetCustomer(parameters.CustomerId).Result;
                    Console.WriteLine($"Customer received: {JsonSerializer.Serialize(customer)}");

                    break;

                case Parameters.Command.CreateCustomer:
                        var newCustomer = new Customer
                        {
                            Id = Guid.NewGuid(),
                            FirstName = _randomStringGenerator.GenerateRandomString(),
                            LastName = _randomStringGenerator.GenerateRandomString()
                        };
                        
                        Console.WriteLine($"Creating customer '{JsonSerializer.Serialize(newCustomer)}'");
                        _customerConnector.CreateCustomer(newCustomer).Wait();
                        Console.WriteLine("Customer has been created successfully");

                        break;
                }
            });
    }
}