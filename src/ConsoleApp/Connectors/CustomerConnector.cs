﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using WebApi.Contracts.Entities;

namespace ConsoleApp.Connectors
{
    public  class CustomerConnector : ICustomerConnector
    {
        private readonly HttpClient _httpClient;

        public CustomerConnector(
           HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public Task<Customer> GetCustomer(Guid id)
        {
            return _httpClient.GetFromJsonAsync<Customer>($"customers/{id}", new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
        }

        public Task CreateCustomer(Customer customer)
        {
            return  _httpClient.PostAsJsonAsync($"customers", customer);
        }
    }
}
