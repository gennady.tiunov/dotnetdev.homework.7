﻿using DataAccess.Entities;

namespace DataAccess.Contracts
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
