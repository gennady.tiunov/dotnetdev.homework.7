using System.ComponentModel.DataAnnotations;

namespace WebApi.Contracts.Entities
{
    public class Customer
    {
        public Guid Id { get; init; }

        [Required]
        public string FirstName { get; init; }

        [Required]
        public string LastName { get; init; }
    }
}