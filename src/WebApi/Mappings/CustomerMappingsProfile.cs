﻿using AutoMapper;
using WebApi.Contracts.Entities;

namespace WebApi.Mappings
{
    public class CustomerMappingProfile : Profile
    {
        public CustomerMappingProfile()
        {
            CreateMap<Customer, Domain.Entities.Customer>().ReverseMap();
        }
    }
}
