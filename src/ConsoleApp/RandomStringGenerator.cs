﻿using System;
using System.Linq;

namespace ConsoleApp
{
	public class RandomStringGenerator : IRandomStringGenerator
	{
		private readonly Random _random = new Random();
		private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		private readonly sbyte _stringLength;

		public RandomStringGenerator(sbyte stringLength)
		{
			_stringLength = stringLength;
		}

		public string GenerateRandomString()
		{
			return new string(Enumerable.Repeat(Chars, _stringLength)
				.Select(str => str[_random.Next(str.Length)])
				.ToArray());
		}
    }
}
