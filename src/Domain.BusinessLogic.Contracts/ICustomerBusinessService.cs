﻿using Domain.Entities;

namespace Domain.BusinessLogic.Contracts
{
    public interface ICustomerBusinessService : IBusinessService<Customer>
    {
        Task<IEnumerable<Customer>> GetAllAsync();

        Task<Customer> GetByIdAsync(Guid id);

        Task AddAsync(Customer entity);

        Task UpdateAsync(Customer entity);

        Task DeleteAsync(Guid id);
    }
}