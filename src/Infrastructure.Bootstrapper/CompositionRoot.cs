﻿using DataAccess;
using DataAccess.Contracts;
using Domain.BusinessLogic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.EntityFramework
{
    public static class CompositionRoot
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            return services
                .RegisterDataContext()
                .RegisterRepositories()
                .RegisterBusinessServices();
        }

        private static IServiceCollection RegisterDataContext(this IServiceCollection services)
        {
            services.AddDbContext<DataContext>(builder =>
            {
                builder.UseInMemoryDatabase("CustomersDb");
            }, ServiceLifetime.Scoped);

            return services;
        }

        private static IServiceCollection RegisterRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICustomerRepository, CustomerRepository>();

            return serviceCollection;
        }

        private static IServiceCollection RegisterBusinessServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<Domain.BusinessLogic.Contracts.ICustomerBusinessService, CustomerBusinessService>();

            return serviceCollection;
        }
    }
}