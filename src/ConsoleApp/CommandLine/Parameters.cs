﻿using CommandLine;
using System;
using System.Collections.Generic;

namespace ConsoleApp.CommandLine
{
    public class Parameters
    {
        public enum Command
        {
            GetCustomer,
            CreateCustomer
        }

        [Option(
            "command",
            Required = true,
            Separator = ',',
            HelpText = "Commands: GetCustomer,CreateCustomer")]
        public IList<Command> Commands { get; set; }

        [Option(
            "id",
            Required = false,
            HelpText = "Id to get the respective customer by")]
        public Guid CustomerId { get; set; }
    }
}
