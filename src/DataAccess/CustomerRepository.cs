﻿using DataAccess.Contracts;
using DataAccess.Entities;

namespace DataAccess
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DataContext dataContext) : base(dataContext)
        {
        }
    }
}