﻿namespace DataAccess.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}