﻿using System;
using System.Threading.Tasks;
using WebApi.Contracts.Entities;

namespace ConsoleApp.Connectors
{
    public interface ICustomerConnector
    {
        Task<Customer> GetCustomer(Guid id);

        Task CreateCustomer(Customer customer);
    }
}